<?php
    session_start();

    if (isset($_POST['reset'])) {
      session_destroy();
      $_SESSION['grille'] = [];
      $_SESSION['ship'] = [];
    }

  $ligne = ['A', 'B', 'C', 'D', 'E', 'F'];
  $colone = [1,2,3,4,5, 6, 7, 8, 9, 10];

  if(empty($_SESSION['grille'])){

    $grille = array();
    $grille = init($ligne, $colone, $grille);
    $_SESSION['ship'] = generateShip($colone, $ligne);
     $_SESSION['grille'] = $grille;


  } else {

    $grille = $_SESSION['grille'];

  }




  function init($ligne, $colone, $grille){
    for($i = 0; $i < count($ligne); $i++){
      // $grille[$ligne[$i]] = [];
      for($a = 0; $a < count($colone); $a++){

          $b = $ligne[$i] . $colone[$a];
          $grille[$b] = 0;
      }
    }
    return $grille;
  }

  function generateShip($colone, $ligne){
    $case = $ligne[rand(0,5)] . $colone[rand(0,9)];
    return $case;
  }

  function attaque($case){
    if($case === $_SESSION['ship']){
      $_SESSION['grille'][$case] = 2;
    } else {
      $_SESSION['grille'][$case] = 1;
    }

    return $_SESSION['grille'];
  }
 ?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
      body{
        font-family: sans-serif;
      }
      li{
        float: left;
        width: 10%;
        border: 1px solid #fff;
        box-sizing:border-box;
        -webkit-box-sizing:border-box;
        list-style-type: none;
        height: 60px;
        text-align: center;
        line-height: 60px;
      }
      ul{
        padding: 0;
      }
      .vert {
        background-color: green;
        color: #fff;
      }
      .orange{
        background-color: orange;
      }
      .red{
        background-color: red;
      }


    </style>
  </head>
  <body>

    <?php



    ?>
      <ul>
    <?php

    if(isset($_POST['attaque']) && isset($_POST['case'])){
      $grille = attaque($_POST['case']);
    }


    foreach ($grille as $key => $value) {
      if($value == 0){
        $class = 'vert';
      } elseif ($value == 1 ) {
        $class = "orange";
      } else {
        $class = 'red';
      }
      ?>
        <li class="<?= $class ?>"><?= $key ?></li>
      <?php
    }


    ?>
  </ul>

  <form class="" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <select name="case">
      <?php
        foreach ($grille as $key => $value) {
            if($value === 0){


          ?>
            <option value="<?= $key ?>"><?= $key ?></option>
          <?php
        }
        }

      ?>
    </select>
    <input type="submit" name="attaque" value="attaquer">
  </form>

  <form class="" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="submit" name="reset" value="reset">
  </form>



  </body>
</html>
